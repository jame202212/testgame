import 'package:flutter/material.dart';
import 'package:live_game_roulette/flame/game_roulette.dart';
import 'package:live_game_roulette/live_game_manager.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key, @required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = -1;
  int _degree = 0;
  int style;
  @override
  void initState() {
    super.initState();
    // style = LiveGameManager.style;
    //
    // Timer.periodic(Duration(seconds: 15), (timer) {
    //   _getDegree();
    // });
  }

  void _getDegree() {
    setState(() {
      int counter = LiveGameManager.randomInt(min: 0, max: 37);
      LiveGameManager.rouletteStart(
          winCount: counter,
          onFail: (String msg) {
            print(msg);
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: const Padding(
        padding: EdgeInsets.all(8.0),
        child: GameRoulette(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _getDegree,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
