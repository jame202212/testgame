/*
  author: Jpeng
  email: peng8350@gmail.com
  time:2019-7-26 3:30
*/

library flutter_gifimage;

import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui show Codec, Image;
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class GifImageInfo /*extends ImageInfo*/ {
  GifImageInfo({this.id, this.image, this.scale = 1.0, this.duration});

  final int id;

  final ui.Image image;
  final double scale;
  final Duration duration;
}

class GifCache {
  final Map<String, List<GifImageInfo>> caches = Map();

  void clear() {
    caches.clear();
  }

  bool evict(Object key) {
    final List<GifImageInfo> pendingImage = caches.remove(key);
    if (pendingImage != null) {
      return true;
    }
    return false;
  }
}

class GifController extends AnimationController {
  GifController(
      {@required TickerProvider vsync,
      double value = 0.0,
      Duration reverseDuration,
      Duration duration,
      AnimationBehavior animationBehavior})
      : super.unbounded(
            value: value,
            reverseDuration: reverseDuration,
            duration: duration,
            animationBehavior: animationBehavior ?? AnimationBehavior.normal,
            vsync: vsync);

  @override
  void reset() {
    value = 0.0;
  }
}

class GifImage extends StatefulWidget {
  GifImage({
    @required this.image,
    @required this.controller,
    this.semanticLabel,
    this.excludeFromSemantics = false,
    this.width,
    this.height,
    this.onFetchCompleted,
    this.color,
    this.colorBlendMode,
    this.fit,
    this.alignment = Alignment.center,
    this.repeat = ImageRepeat.noRepeat,
    this.centerSlice,
    this.matchTextDirection = false,
    this.gaplessPlayback = false,
    this.onCompleted,
  });
  final VoidCallback onFetchCompleted;
  final GifController controller;
  final ImageProvider image;
  final double width;
  final double height;
  final Color color;
  final BlendMode colorBlendMode;
  final BoxFit fit;
  final AlignmentGeometry alignment;
  final ImageRepeat repeat;
  final Rect centerSlice;
  final bool matchTextDirection;
  final bool gaplessPlayback;
  final String semanticLabel;
  final bool excludeFromSemantics;
  final VoidCallback onCompleted;

  @override
  State<StatefulWidget> createState() {
    return new GifImageState();
  }

  Duration _duration;
  double _frameCount;

  Future<double> get frameCount async {
    if (_frameCount != null) {
      return _frameCount;
    }

    Completer<double> comp = Completer();
    fetchGif(image).then((_infos) {
      _frameCount = _infos.length?.toDouble() ?? 0;
      comp.complete(_frameCount);
    });
    return comp.future;
  }

  Future<Duration> get duration async {
    if (_duration != null) return _duration;
    Completer<Duration> comp = Completer();
    fetchGif(image).then((_infos) {
      _duration = Duration();
      for (var item in _infos) {
        _duration += item.duration;
      }
      comp.complete(_duration);
    });
    return comp.future;
  }

  static GifCache cache = GifCache();

  static int GifFrameIndex = 0;
}

class GifImageState extends State<GifImage> {
  List<GifImageInfo> _infos;
  int _curIndex = 0;
  bool _fetchComplete = false;

  GifImageInfo get _imageInfo {
    if (!_fetchComplete) return null;
    return _infos == null ? null : _infos[_curIndex];
  }

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_listener);
  }

  @override
  void dispose() {
    super.dispose();
    widget.controller.removeListener(_listener);
  }

  RawImage _curRawImage;

  Map<int, RawImage> cacheRawImage = {};

  buildRawImage() {
    final RawImage image = new RawImage(
      image: _imageInfo?.image,
      width: widget.width,
      height: widget.height,
      scale: _imageInfo?.scale ?? 1.0,
      color: widget.color,
      colorBlendMode: widget.colorBlendMode,
      fit: widget.fit,
      alignment: widget.alignment,
      repeat: widget.repeat,
      centerSlice: widget.centerSlice,
      matchTextDirection: widget.matchTextDirection,
    );

    return image;
  }

  setCurIndex(double i) async {
    _curIndex = i.toInt();
    if (_infos.length <= _curIndex) {
      if (this.widget.onCompleted != null) this.widget.onCompleted();
      _curIndex = 0;
    }
    _curRawImage = buildRawImage();
  }

  fetchGifThen(imageInfors) {
    if (mounted)
      setState(() {
        _infos = imageInfors;
        _fetchComplete = true;
        setCurIndex(widget.controller.value);

        if (widget.onFetchCompleted != null) {
          widget.onFetchCompleted();
        }
      });
  }

  @override
  void didUpdateWidget(GifImage oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.image != oldWidget.image) {
      fetchGif(widget.image).then(fetchGifThen);
    }
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.removeListener(_listener);
      widget.controller.addListener(_listener);
    }
  }

  static int currentTimeMillis() {
    return new DateTime.now().millisecondsSinceEpoch;
  }

  void _listener() {
    if (_curIndex != widget.controller.value.toInt() && _fetchComplete) {
      if (mounted)
        setState(() {
          setCurIndex(widget.controller.value);
        });
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_infos == null) {
      fetchGif(widget.image).then(fetchGifThen);
    }
  }

  @override
  Widget build(BuildContext context) {
    return _curRawImage ??
        RawImage(
          image: null,
          width: widget.width,
          height: widget.height,
          scale: _imageInfo?.scale ?? 1.0,
          color: widget.color,
          colorBlendMode: widget.colorBlendMode,
          fit: widget.fit,
          alignment: widget.alignment,
          repeat: widget.repeat,
          centerSlice: widget.centerSlice,
          matchTextDirection: widget.matchTextDirection,
        );
  }
}

final HttpClient _sharedHttpClient = HttpClient()..autoUncompress = false;

HttpClient get _httpClient {
  HttpClient client = _sharedHttpClient;
  assert(() {
    if (debugNetworkImageHttpClientProvider != null)
      client = debugNetworkImageHttpClientProvider();
    return true;
  }());
  return client;
}

Future<List<GifImageInfo>> fetchGif(ImageProvider provider) async {
  List<GifImageInfo> infos = [];
  dynamic data;
  String key = provider is NetworkImage
      ? provider.url
      : provider is AssetImage
          ? provider.assetName
          : provider is MemoryImage
              ? provider.bytes.toString()
              : "";
  if (GifImage.cache.caches.containsKey(key)) {
    infos = GifImage.cache.caches[key];
    return infos;
  }
  if (provider is NetworkImage) {
    final Uri resolved = Uri.base.resolve(provider.url);
    final HttpClientRequest request = await _httpClient.getUrl(resolved);
    provider.headers?.forEach((String name, String value) {
      request.headers.add(name, value);
    });
    final HttpClientResponse response = await request.close();
    data = await consolidateHttpClientResponseBytes(
      response,
    );
  } else if (provider is AssetImage) {
    AssetBundleImageKey key = await provider.obtainKey(ImageConfiguration());
    data = await key.bundle.load(key.name);
  } else if (provider is FileImage) {
    data = await provider.file.readAsBytes();
  } else if (provider is MemoryImage) {
    data = provider.bytes;
  }
  ui.Codec codec;
  try {
    codec = await PaintingBinding.instance
        .instantiateImageCodec(data.buffer.asUint8List());
  } catch (e) {}
  infos = [];
  if (codec != null) {
    int totalDuration = 0;
    for (int i = 0; i < codec.frameCount; i++) {
      int id = ++GifImage.GifFrameIndex;
      FrameInfo frameInfo = await codec.getNextFrame();
      var d = frameInfo.duration;
      var md = d.inMilliseconds < 100 ? 100 : d.inMilliseconds;
      infos.add(GifImageInfo(
        id: id,
        image: frameInfo.image,
        duration: Duration(milliseconds: md),
      ));
      totalDuration = totalDuration + md;
    }
  }

  if (key != "") {
    GifImage.cache.caches.putIfAbsent(key, () => infos);
  }
  return infos;
}
