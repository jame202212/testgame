import 'dart:io';

import 'package:flutter/material.dart';
import 'package:live_game_roulette/util/flutter_gifanimate.dart';

class WebpPlayerWidget extends StatefulWidget {
  WebpPlayerWidget({
    Key key,
    this.url,
    this.directlyUrl,
    this.semanticLabel,
    this.excludeFromSemantics = false,
    this.width,
    this.height,
    this.onCompleted,
    this.color,
    this.colorBlendMode,
    this.fit,
    this.vsync,
    this.alignment = Alignment.center,
    this.repeat = ImageRepeat.noRepeat,
    this.centerSlice,
    this.matchTextDirection = false,
    this.gaplessPlayback = false,
    this.isReset = false,
  }) : super(key: key);

  final Map<String, dynamic> url;
  final String directlyUrl; //仍然需要支持使用直接的本地url
  final VoidCallback onCompleted;
  final double width;
  final double height;
  final Color color;
  final BlendMode colorBlendMode;
  final BoxFit fit;
  final AlignmentGeometry alignment;
  final ImageRepeat repeat;
  final Rect centerSlice;
  final bool matchTextDirection;
  final bool gaplessPlayback;
  final String semanticLabel;
  final bool excludeFromSemantics;
  final bool isReset;
  final TickerProvider vsync;

  @override
  _WPState createState() => _WPState();
}

class _WPState extends State<WebpPlayerWidget> with TickerProviderStateMixin {
  GifImage _gifImage;
  Widget _giftWidget;
  GifController _gifCtrl;
  ImageProvider<dynamic> _image;

  _getImagePara() async {
    if (mounted) {
      var data;
      if (_gifImage != null) {
        data = {};
        data['frameCount'] = await _gifImage?.frameCount;
        data['duration'] = await _gifImage?.duration;
      }
      return data;
    }
  }

  @override
  void initState() {
    super.initState();
    _gifCtrl = GifController(vsync: widget.vsync ?? this);
    _gifCtrl.addListener(() {
      if (_gifCtrl.isCompleted) {
        if (widget.onCompleted != null) {
          widget.onCompleted();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if ((widget.url == null || widget.url.isEmpty) &&
        (widget.directlyUrl == null || widget.directlyUrl.isEmpty)) {
      return Container();
    }
    if (widget.isReset) {
      //代表要重新建構
      _gifImage = null;
    }

    if (_gifImage == null) {
      if (widget.directlyUrl == null || widget.directlyUrl.isEmpty) {
        if (widget.url["type"] == 0) {
          _image = NetworkImage(widget.url["img"]);
        } else if (widget.url["type"] == 3) {
          _image = FileImage(File(widget.url["img"]));
        } else {
          _image = AssetImage(widget.url["img"]);
        }

        var lowUrl = widget.url["img"].toLowerCase();
        if (!(lowUrl.endsWith('.gif') || lowUrl.endsWith('.webp'))) {
          return Image(
              width: widget.width, height: widget.height, image: _image);
        }
      } else {
        if (widget.directlyUrl.toLowerCase().indexOf('http') > -1 ||
            widget.directlyUrl.toLowerCase().indexOf("https") > -1) {
          _image = NetworkImage(widget.directlyUrl);
        } else if (widget.directlyUrl.startsWith("/")) {
          _image = FileImage(File(widget.directlyUrl));
        } else {
          _image = AssetImage(widget.directlyUrl);
        }

        var lowUrl = widget.directlyUrl.toLowerCase();
        if (!(lowUrl.endsWith('.gif') || lowUrl.endsWith('.webp'))) {
          return Image(
              width: widget.width, height: widget.height, image: _image);
        }
      }

      _gifImage = GifImage(
        width: widget.width,
        height: widget.height,
        fit: widget.fit,
        alignment: widget.alignment,
        controller: _gifCtrl,
        image: _image,
        onCompleted: widget.onCompleted,
      );
      _gifCtrl.value = 0;
      _giftWidget = RepaintBoundary(child: _gifImage);
    }

    return FutureBuilder(
        future: _getImagePara(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            if (widget.onCompleted == null) {
              _gifCtrl.repeat(
                  min: 0,
                  max: snapshot.data['frameCount'],
                  period: snapshot.data['duration']);
            } else {
              _gifCtrl.animateTo(snapshot.data['frameCount'],
                  duration: snapshot.data['duration']);
            }
          }
          return _giftWidget;
        });
  }

  @override
  void dispose() {
    _gifCtrl?.stop();
    _gifCtrl?.dispose();
    _gifImage = null;
    _image = null;
    super.dispose();
  }
}
