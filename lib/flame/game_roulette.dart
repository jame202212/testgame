import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:live_game_roulette/flame/game_roulette_half.dart';
import 'package:live_game_roulette/flame/game_roulette_whole.dart';

class GameRoulette extends StatelessWidget {
  const GameRoulette(
      {Key key, this.width = 300, this.height = 120, this.type = 2})
      : super(key: key);

  final double width;
  final double height;
  final int type;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: width,
        height: height,
        child: type == 2
            ? Stack(
                alignment: Alignment.center,
                children: [
                  GameWidget(game: GameRouletteHalf()),
                  ShiningEffect(
                    height: height,
                    width: width,
                  )
                ],
              )
            : GameWidget(game: GameRouletteWhole()),
      ),
    );
  }
}
