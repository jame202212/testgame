import 'dart:math';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/palette.dart';
import 'package:live_game_roulette/flame/game_roulette_half.dart';
import 'package:live_game_roulette/flame/game_roulette_whole.dart';
import 'package:live_game_roulette/flame/roulette_details.dart';
import 'package:live_game_roulette/live_game_manager.dart';

class BallDestionation extends RectangleComponent {
  @override
  Future<void> onLoad() async {
    // debugMode = true;
    size = Vector2.all(5);
    anchor = Anchor.center;

    position = Vector2(
      pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].x,
      pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].y,
    );
    paint = BasicPalette.transparent.paint();
    add(RectangleHitbox(size: size));
    super.onLoad();
  }
}

class StopperBallComponent extends RectangleComponent {
  @override
  Future<void> onLoad() async {
    // debugMode = true;
    size = Vector2(10.0, 10.0);
    anchor = Anchor.center;

    List _getList = cutListByIndex(stopperGap);

    int index;
    index = rouletteNumbers.indexOf(LiveGameManager.resultNum);
    if (index >= 0 && index <= stopperGap) {
      index = _getList[index];

      position = Vector2(
        pointer[index].x,
        pointer[index].y,
      );
    } else {
      position = Vector2(
        pointer[index - stopperGap].x,
        pointer[index - stopperGap].y,
      );
    }

    paint = BasicPalette.transparent.paint();
    add(RectangleHitbox(size: size));
    super.onLoad();
  }
}

///style one stopper
class StopperSOne extends RectangleComponent {
  @override
  Future<void> onLoad() async {
    // debugMode = true;
    size = Vector2(10.0, 10.0);
    anchor = Anchor.center;
    position = Vector2(
      style1Pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].x,
      style1Pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].y,
    );
    paint = BasicPalette.transparent.paint();
    add(RectangleHitbox(size: size));
    super.onLoad();
  }
}

class StopperSTwo extends RectangleComponent {
  @override
  Future<void> onLoad() async {
    // debugMode = true;
    size = Vector2(10.0, 10.0);
    anchor = Anchor.center;
    int index = rouletteNumbers.indexOf(LiveGameManager.resultNum);

    int randomLength = LiveGameManager.randomInt(min: 2, max: 4);
    List ranIdx = [];
    for (int i = 0; i < randomLength; i++) {
      ranIdx.add(i);
      if (index == rouletteNumbers.length - randomLength) {
        position = Vector2(
          pointer[i].x,
          pointer[i].y,
        );
      } else {
        position = Vector2(
          pointer[index + randomLength].x,
          pointer[index + randomLength].y,
        );
      }
    }

    paint = BasicPalette.transparent.paint();
    add(RectangleHitbox(size: size));
    super.onLoad();
  }
}

class StopperSThree extends RectangleComponent with CollisionCallbacks {
  StopperSThree(this.part) : super();

  final double part;
  double _radius;
  @override
  Future<void> onLoad() async {
    // debugMode = true;
    size = Vector2(10.0, 10.0);
    anchor = Anchor.center;

    _radius = LiveGameManager.circleWheelRadius / 2;

    int index = rouletteNumbers.indexOf(LiveGameManager.resultNum);
    final angle = Transform2D.tau * (index / 37);

    if (part == 1) {
      position = Vector2(x = _radius + (_radius + 25) * sin(angle),
          y = _radius + (_radius + 25) * cos(angle));
    }
    if (part == 2) {
      position = Vector2(x = _radius + _radius * sin(angle),
          y = _radius + _radius * cos(angle));
    }

    paint = BasicPalette.transparent.paint();
    add(RectangleHitbox(size: size));
    super.onLoad();
  }
}

///stopper for half roulette
class HalfRouletterWinnerNumber extends RectangleComponent
    with CollisionCallbacks {
  HalfRouletterWinnerNumber(this.part) : super();

  final double part;

  double _radius = 75;
  int gap = 16;
  @override
  Future<void> onLoad() async {
    // debugMode = true;
    int index = rouletteNumbers.indexOf(LiveGameManager.resultNum);
    double angle = Transform2D.tau * (index / 37);

    List _getList = cutListByIndex(gap);

    if (part == 1) {
      position = Vector2(_radius + (_radius - 10) * sin(angle),
          _radius + (_radius - 10) * cos(angle));
    } else if (part == 2) {
      if (index >= 0 && index <= gap) {
        index = _getList[index];
        angle = Transform2D.tau * (index / 37);
        position = Vector2(_radius + (_radius - 10) * sin(angle),
            _radius + (_radius - 10) * cos(angle));
      } else {
        angle = Transform2D.tau * ((index - gap) / 37);
        position = Vector2(_radius + (_radius - 10) * sin(angle),
            _radius + (_radius - 10) * cos(angle));
      }
    }

    size = Vector2.all(5);
    anchor = Anchor.center;
    paint = BasicPalette.transparent.paint();
    add(RectangleHitbox(size: size));
    super.onLoad();
  }

  @override
  void onCollisionStart(
      Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);
    if (other is Arrow) {
      if (Arrow.showWinnerResult && part == 1 && WheelComponent.hitStopper) {
          WheelComponent.endAnimation = true;
          WheelComponent.hitStopper = false;

      }
      if (Arrow.showWinnerResult && part == 2) {
        WheelComponent.hitBox = true;
        WheelComponent.hitStopper= true;
      }
    }
  }
}
