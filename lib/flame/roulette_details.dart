import 'package:live_game_roulette/flame/game_roulette_half.dart';

class Pointer {
  Pointer(this.x, this.y) : super();
  double x;
  double y;
}

List<int> reverserRouletteNumbers = [
  0,
  32,
  15,
  19,
  4,
  21,
  2,
  25,
  17,
  34,
  6,
  27,
  13,
  36,
  11,
  30,
  8,
  23,
  10,
  5,
  24,
  16,
  33,
  1,
  20,
  14,
  31,
  9,
  22,
  18,
  29,
  7,
  28,
  12,
  35,
  3,
  26,
];

List<int> rouletteNumbers = [
  0,
  26,
  3,
  35,
  12,
  28,
  7,
  29,
  18,
  22,
  9,
  31,
  14,
  20,
  1,
  33,
  16,
  24,
  5,
  10,
  23,
  8,
  30,
  11,
  36,
  13,
  27,
  6,
  34,
  17,
  25,
  2,
  21,
  4,
  19,
  15,
  32
];

///max 36, int = len
///ex: 36 - 10 = 26 array start at 26 end to 36
///use: to add slowpoint for style 1
cutListByIndex(int len) {
  List<int> list = [];
  list.clear();

  int maxValue = 36;
  int holder = maxValue - len;

  for (int i = 0; i <= len; i++) {
    holder++;
    list.add(holder);
  }
  print(list);
  return list;
}

createIndex(int len) {
  List<int> list = [];
  list.clear();

  for (int i = 0; i < len; i++) {
    list.add(0);
  }

  print(list);
  return list;
}

List<WinnerColor> colorList = [];

createColorPattern() {
  for (int i = 0; i < rouletteNumbers.length; i++) {
    if (i == 0) {
      colorList.add(WinnerColor.green);
    } else if (i.isOdd) {
      colorList.add(WinnerColor.black);
    } else {
      colorList.add(WinnerColor.red);
    }
  }
}
