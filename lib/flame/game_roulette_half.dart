import 'dart:math';

import 'package:flame/cache.dart';
import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flame/game.dart';
import 'package:flame/rendering.dart';
import 'package:flutter/material.dart';
import 'package:live_game_roulette/flame/hitbox_components.dart';
import 'package:live_game_roulette/flame/roulette_details.dart';
import 'package:live_game_roulette/live_game_manager.dart';
import 'package:live_game_roulette/util/web_player_widget.dart';

Function showShinningEffect = () {};

class ShiningEffect extends StatefulWidget {
  const ShiningEffect({Key key, this.width, this.height}) : super(key: key);
  final double width;
  final double height;

  @override
  State<ShiningEffect> createState() => _ShiningEffectState();
}

class _ShiningEffectState extends State<ShiningEffect> {
  bool effect = false;

  @override
  void initState() {
    super.initState();
    showShinningEffect = () {
      if (mounted) {
        setState(() {
          effect = !effect;
        });
      }
    };
  }

  @override
  void dispose() {
    super.dispose();
    showShinningEffect = () {};
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: effect,
      child: WebpPlayerWidget(
        key: UniqueKey(),
        fit: BoxFit.fill,
        height: (widget.width * 0.34),
        width: (widget.width * 0.34),
        alignment: Alignment.center,
        directlyUrl: 'packages/live_game_roulette/assets/img/shining.webp',
      ),
    );
  }
}

class GameRouletteHalf extends FlameGame
    with HasCollisionDetection, HasGameRef {
  static final image =
      Images(prefix: "packages/live_game_roulette/assets/img/");

  @override
  Color backgroundColor() => Colors.teal;

  @override
  void onMount() {
    createColorPattern();
    // debugMode = true;
    super.onMount();
    camera.viewport = FixedResolutionViewport(Vector2(75, 30));
    final wheel = RouletteWheel()
      ..position = Vector2.all(75)
      ..x = 75 / 2;
    add(wheel);

    final wheelShadow = WheelShadow()
      ..position = Vector2(75, 18)
      ..x = 75 / 2;

    add(wheelShadow);
  }
}

class Arrow extends SpriteComponent {
  static bool showWinnerResult = false;
  static bool showWinnerNumberComponent = false;
  @override
  Future<void> onLoad() async {
    // debugMode = true;
    sprite =
        Sprite(await GameRouletteHalf.image.load('roulette_arrow_down.png'));

    anchor = Anchor.center;
    size = Vector2.all(5);
    add(RectangleHitbox(size: size / 2));
    return super.onLoad();
  }
}

class WheelShadow extends SpriteComponent {
  @override
  Future<void> onLoad() async {
    sprite = Sprite(await GameRouletteHalf.image.load('horizontal_shadow.png'));

    anchor = Anchor.center;
    size = Vector2(80, 25);
    return super.onLoad();
  }
}

class RouletteWheel extends PositionComponent {
  RouletteWheel()
      : super(
          anchor: Anchor.center,
          size: Vector2.all(150),
        );
  WheelComponent wheel = WheelComponent();
  WinnerNumber winComp;
  bool showOnceResult = false;

  @override
  Future<void> onLoad() async {
    // debugMode = true;

    add(wheel);
    final arrow = Arrow()..position = Vector2(75, 11);
    add(arrow);
    return super.onLoad();
  }

  @override
  void update(double dt) {
    super.update(dt);

    if (Arrow.showWinnerNumberComponent && !showOnceResult) {
      add(TimerComponent(
          period: 0.5,
          onTick: () {
            winComp = WinnerNumber(
                win: colorList[
                    rouletteNumbers.indexOf(LiveGameManager.resultNum)]);
            add(winComp);
          }));
      showOnceResult = true;
    } else {
      if (contains(winComp) && LiveGameManager.playAgain) {
        remove(winComp);
        showOnceResult = false;
        Arrow.showWinnerNumberComponent = false;
        showShinningEffect();
      }
    }
  }
}

class WheelComponent extends SpriteComponent {
  WheelComponent()
      : super(
          anchor: Anchor.center,
          size: Vector2.all(150),
        );

  bool callSpinTimer = false;
  bool correctAngle = false;
  double toBlur = 0.0;
  double pie;
  static double speed = 5;

  HalfRouletterWinnerNumber destination;
  HalfRouletterWinnerNumber slowDownToDestination;
  SpriteComponent showMiddleWinner;
  bool addStopper = false;
  static bool hitStopper = false;
  static bool endAnimation = false;
  static bool hitBox = false;
  bool isRotateNegative = false;
  
  @override
  Future<void> onLoad() async {
    // debugMode = true;
    sprite = Sprite(await GameRouletteHalf.image.load('number_wheel.png'));
    position = Vector2.all(75);
    anchor = Anchor.center;
    size = size;

    decorator.addLast(PaintDecorator.blur(0));
    return super.onLoad();
  }

  @override
  void update(double dt) async {
    super.update(dt);

    if (LiveGameManager.playAgain && LiveGameManager.resultNum != -1) {
      if (!addStopper) {
        sprite = Sprite(await GameRouletteHalf.image.load('number_wheel.png'));

        angle = 0.0;
        if (contains(destination)) {
          remove(destination);
        }
        if (contains(slowDownToDestination)) {
          remove(slowDownToDestination);
        }
        add(destination = HalfRouletterWinnerNumber(1));
        add(slowDownToDestination = HalfRouletterWinnerNumber(2));
        addStopper = true;
      }

      angle += speed * dt;
      if (toBlur <= 0.6) {
        decorator.replaceLast(PaintDecorator.blur(toBlur));
        toBlur += 0.3 * dt;
      }
      if (!callSpinTimer) {
        add(TimerComponent(period: 2.5, onTick: () => speed = 3.2));
        add(TimerComponent(period: 3, onTick: () => speed = 2.7));
        add(TimerComponent(period: 3.5, onTick: () => speed = 2.2));
        add(TimerComponent(
            period: 4,
            onTick: () {
              Arrow.showWinnerResult = true;
            }));
        correctAngle = false;
        callSpinTimer = true;

        y = 75;
      }

      if (hitBox) {
        if(speed >= 0.18){

          speed -= 1 * dt;
        }else if(speed <= 0.13 && speed >= 0.002){
          speed -= 0.135 * dt;
        }else{
            if(!speed.isNegative && !isRotateNegative){
              speed -= 0.025 * dt;
            }else{
              isRotateNegative = true;
              speed = 1 * dt;
            }
        }
        // print(speed);
      }

      if (endAnimation) {
        add(TimerComponent(period: 1, onTick: (){

          LiveGameManager.playAgain = false;
          Arrow.showWinnerNumberComponent = true;
        }));
      }
    } else {
      endAnimation = false;
      addStopper = false;
      hitBox = false;
      isRotateNegative = false;
      if (!correctAngle) {
        y = 75;
        decorator.replaceLast(PaintDecorator.blur(0));

        add(TimerComponent(period: 1, onTick: () => toBlur = 0.0));

        sprite =
            Sprite(await GameRouletteHalf.image.load('number_wheel_rev.png'));
        if (y == 75) {
          y += 0.5;
        }
        pie = (360 / 37);
        angle = (pie * pi / 180) *
            rouletteNumbers.indexOf(LiveGameManager.resultNum).toDouble();

        correctAngle = true;
      }
      if (toBlur <= 0.6) {
        decorator.replaceLast(PaintDecorator.blur(toBlur));
        toBlur += 0.3 * dt;
      }
      speed = 5;
      callSpinTimer = false;
      Arrow.showWinnerResult = false;
    }
  }
}

class WinnerNumber extends PositionComponent {
  WinnerNumber({this.win}) : super(anchor: Anchor.center);
  SpriteComponent winner;
  WinnerColor win;
  SpriteComponent highlightedBorder;
  TextComponent text;
  bool light = false;
  bool addLightOnce = false;
  EffectController _controller;

  double lightUpDuration = 0.25;

  String winnerColor(WinnerColor win) {
    String color;

    if (win == WinnerColor.red) {
      color = 'red';
    } else if (win == WinnerColor.black) {
      color = 'black';
    } else {
      color = 'green';
    }
    return color;
  }

  @override
  Future<void> onLoad() async {
    // debugMode = true;

    _controller = EffectController(
        duration: 0.5,
        startDelay: 4.5,
        curve: Curves.ease,
        repeatCount: 2,
        alternate: true);

    text = TextComponent(
        text: LiveGameManager.resultNum.toString(),
        anchor: Anchor.center,
        size: size,
        textRenderer: TextPaint(
            style: const TextStyle(color: Colors.white, fontSize: 8, shadows: [
          Shadow(color: Colors.black87, offset: Offset(0, 1), blurRadius: 15)
        ])),
        position: Vector2(75, 15))
      ..add(ScaleEffect.to(
          Vector2.all(1.5), EffectController(duration: lightUpDuration),
          onComplete: () async {
        text.add(ScaleEffect.by(Vector2.all(1.1), _controller));
      }));

    winner = SpriteComponent(
      sprite:
          Sprite(await GameRouletteHalf.image.load('${winnerColor(win)}.png')),
      anchor: Anchor.center,
      size: Vector2.all(15),
      position: Vector2(75, 15),
    )
      ..add(OpacityEffect.to(0, EffectController(duration: 0)))
      ..add(OpacityEffect.fadeIn(
        EffectController(
          duration: lightUpDuration,
        ),
      ))
      ..add(ScaleEffect.to(
          Vector2.all(1.5), EffectController(duration: lightUpDuration),
          onComplete: () {
        light = true;
        winner.add(ScaleEffect.by(
            Vector2.all(1.1), //Star size when animating
            _controller));
      }));
    add(winner);
    add(text);
    //
    add(TimerComponent(
        period: lightUpDuration,
        onTick: () {
          showShinningEffect();
        }));
    return super.onLoad();
  }

  @override
  void update(double dt) async {
    super.update(dt);

    if (light) {
      if (!addLightOnce) {
        add(highlightedBorder = SpriteComponent(
            sprite: Sprite(
                await GameRouletteHalf.image.load('border_highlight.png')),
            anchor: Anchor.center,
            size: Vector2.all(18),
            position: Vector2(75, 15),
            scale: Vector2.all(1.5))
          ..add(ScaleEffect.to(
              Vector2.all(1.6), //Star size when animating
              _controller)));

        add(TimerComponent(period: 1, onTick: () => showShinningEffect()));

        add(TimerComponent(period: 2.5, onTick: () => showShinningEffect()));

        addLightOnce = true;
      }
    }
  }
}

enum WinnerColor { red, green, black }
