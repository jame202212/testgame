import 'dart:async';
import 'dart:math';
import 'dart:math' as math;

import 'package:flame/cache.dart';
import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flame/game.dart';
import 'package:live_game_roulette/flame/hitbox_components.dart';
import 'package:live_game_roulette/flame/roulette_details.dart';
import 'package:live_game_roulette/live_game_roulette.dart';

List<Pointer> pointer = [];
List<Pointer> style1Pointer = [];
List<Pointer> style3Pointer = [];
double circleCenter;
double circleRadius;
int stopperGap = 15;

class GameRouletteWhole extends FlameGame with HasCollisionDetection {
  static final image =
      Images(prefix: "packages/live_game_roulette/assets/img/");

  bool addOnce = false;
  bool run = false;

  BackgroundRoulette background;
  RouletteWheel rouletteWheel;
  @override
  void onMount() async {
    LiveGameManager;
    super.onMount();
    // createIndex();
    camera.viewport = FixedResolutionViewport(Vector2(400, 600));
    background = BackgroundRoulette(LiveGameManager.circleOuterRadius)
      ..position = Vector2(200, 300);
    add(background);

    add(SpriteComponent(
        sprite: Sprite(await GameRouletteWhole.image.load('whole_wheel.png')),
        position: Vector2(200, 300),
        anchor: Anchor.center,
        size: Vector2.all(300)));
  }

  @override
  void update(double dt) {
    super.update(dt);
    if (LiveGameManager.playAgain && !run) {
      if (contains(rouletteWheel)) {
        remove(rouletteWheel);
      }

      rouletteWheel = RouletteWheel(LiveGameManager.circleWheelRadius)
        ..position = Vector2(200, 300);
      add(rouletteWheel);
      run = true;
    } else if (!LiveGameManager.playAgain) {
      run = false;
    }
  }
}

class Blinker extends SpriteComponent {
  Blinker({this.index}) : super();
  final double index;
  double pie;
  @override
  Future<void> onLoad() async {
    LiveGameManager.playAgain = false;
    sprite = Sprite(await GameRouletteWhole.image.load('blink.png'));
    anchor = Anchor.center;
    pie = (360 / 37);
    setOpacity(0.0);
    angle = (pie * math.pi / 180) * index;
    super.onLoad();
  }
}

class BackgroundRoulette extends SpriteComponent {
  BackgroundRoulette(double size)
      : super(
          size: Vector2.all(size),
          anchor: Anchor.center,
        );
  @override
  Future<void> onLoad() async {
    sprite = Sprite(await GameRouletteWhole.image.load('outer.png'));
    size = size;
    anchor = Anchor.center;
    return super.onLoad();
  }
}

class RouletteWheel extends PositionComponent {
  RouletteWheel(double size)
      : _radius = size / 2,
        super(
          size: Vector2.all(size),
          anchor: Anchor.center,
        );

  // final List<TextComponent> _numList = [];
  SpriteComponent wheel;
  SpriteComponent shadow;
  SpriteComponent cross;
  SpriteComponent middle;

  Blinker blink;
  Ball ball;
  StopperBallComponent stopperBallComponent = StopperBallComponent();
  BallDestionation ballDestionation = BallDestionation();
  StopperSOne stopperSOne = StopperSOne();
  StopperSTwo stopperSTwo = StopperSTwo();
  StopperSThree stopperSThreeP1;
  StopperSThree stopperSThreeP2;
  StopperSThree stopperSThreeP3;

  final double _radius;
  double speed = 2;
  int winnerIndex;

  bool spinner = false;
  bool spinWheel = false;
  bool showWinner = false;

  @override
  Future<void> onLoad() async {
    circleCenter = _radius;

    wheel = SpriteComponent(
        sprite: Sprite(await GameRouletteWhole.image.load('wheel.png')),
        size: size,
        position: Vector2.all(_radius),
        anchor: Anchor.center);

    add(wheel);

    shadow = SpriteComponent(
        sprite: Sprite(await GameRouletteWhole.image.load('shadow.png')),
        position: Vector2.all(_radius),
        anchor: Anchor.center,
        size: size);

    add(shadow);

    cross = SpriteComponent(
      sprite: Sprite(await GameRouletteWhole.image.load('cross.png')),
      size: size,
      position: Vector2.all(_radius),
      anchor: Anchor.center,
    );
    add(cross);

    middle = SpriteComponent(
      sprite: Sprite(await GameRouletteWhole.image.load('middle.png')),
      size: size,
      position: Vector2.all(_radius),
      anchor: Anchor.center,
    );

    add(middle);

    final markLength = _radius * 0.3;
    for (var i = 0; i < 37; i++) {
      final angle = Transform2D.tau * (i / 37);

      pointer.add(Pointer(_radius + (_radius - markLength) * sin(angle),
          _radius + (_radius - markLength) * cos(angle)));

      if (LiveGameManager.style == 1) {
        if (i >= 4 && i <= 8 ||
            i >= 14 && i <= 17 ||
            i >= 23 && i <= 26 ||
            i >= 32 && i <= 35) {
          style1Pointer.add(Pointer(
              _radius + (_radius - (_radius * 0.6)) * sin(angle),
              _radius + (_radius - (_radius * 0.6)) * cos(angle)));
        } else {
          style1Pointer.add(Pointer(
              _radius + (_radius - (_radius * 0.7)) * sin(angle),
              _radius + (_radius - (_radius * 0.7)) * cos(angle)));
        }
      }
      if (LiveGameManager.style == 3) {
        style3Pointer.add(Pointer(_radius + (_radius + 25) * sin(angle),
            _radius + (_radius + 25) * cos(angle)));
      }
    }

    if (LiveGameManager.playAgain) {
      ball = Ball();
      add(ball..position = Vector2(_radius, _radius));
      add(stopperBallComponent);
      add(ballDestionation);

      if (LiveGameManager.style == 1) {
        add(stopperSOne);
      }
      if (LiveGameManager.style == 2) {
        add(stopperSTwo);
      }
      if (LiveGameManager.style == 3) {
        add(stopperSThreeP1 = StopperSThree(1));
        add(stopperSThreeP2 = StopperSThree(2));
        // add(stopperSThreeP3  = StopperSThree(3));
      }
    }
  }

  @override
  void update(double dt) {
    super.update(dt);
    if (LiveGameManager.playAgain) {
      if (!spinWheel) {
        angle += speed * dt;
        shadow.angle -= speed * dt;
        middle.angle -= speed * dt;
      } else {
        if (!showWinner) {
          blink = Blinker(
              index: reverserRouletteNumbers
                  .indexOf(LiveGameManager.resultNum)
                  .toDouble())
            ..add(OpacityEffect.to(
                0.5,
                EffectController(
                  duration: 0.5,
                  reverseDuration: 0.5,
                  infinite: true,
                )));
          add(blink
            ..size = size
            ..position = Vector2(_radius, _radius));

          showWinner = true;
        }
      }

      if (!spinner) {
        if (contains(blink)) {
          remove(blink);
        }

        double timer;
        if (LiveGameManager.style == 2) {
          timer = 8;
        } else {
          timer = 6.5;
        }

        add(TimerComponent(period: 2, onTick: () => speed = 1.3));
        add(TimerComponent(period: 4, onTick: () => speed = 0.6));
        add(TimerComponent(period: 5.5, onTick: () => speed = 0.3));

        add(TimerComponent(period: timer, onTick: () => spinWheel = true));

        spinner = true;
      }
    } else {
      spinner = false;
    }
  }
}

class Ball extends SpriteComponent
    with HasGameRef<GameRouletteWhole>, CollisionCallbacks {
  double speed = 10;
  double circleRadius = circleCenter + 25;
  double middleBorder = circleCenter - (circleCenter * 0.3);
  bool firstSlowDown = false;
  bool toSecondSlow = false;
  int lap = 0;

  bool stopBallToDestionation = false;
  bool showWinnerBlink = false;
  double ballToMiddle = 50;

  ///style1 conditions
  bool styleOneHitStoppper = false;
  double styleOneSpeed = 0.02;
  bool styleLapped = false;

  ///style2 conditions
  bool styleTwoHitStopper = false;

  @override
  Future<void> onLoad() async {
    // debugMode = true;
    sprite = Sprite(await GameRouletteWhole.image.load('ball.png'));
    anchor = Anchor.center;
    size = Vector2.all(15);
    add(CircleHitbox(radius: 5));
    return super.onLoad();
  }

  @override
  void update(double dt) {
    super.update(dt);

    if (LiveGameManager.playAgain) {
      if (!stopBallToDestionation) {
        angle += speed * dt;
        x = circleCenter + circleRadius * math.sin(angle);
        y = circleCenter + circleRadius * math.cos(angle);
        if (circleRadius >= middleBorder) {
          if (angle >= 5) {
            circleRadius -= ballToMiddle * dt;
            if (!firstSlowDown) {
              speed -= 4;
              ballToMiddle -= 5;
              firstSlowDown = true;
            }
          }
        }
      } else {
        if (LiveGameManager.style == 0) {
          x = pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].x;
          y = pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].y;
        } else if (LiveGameManager.style == 1) {
          final distance = sqrt(pow(circleCenter - (x + width / 2), 2) +
              pow(circleCenter - (y + height / 2), 2));
          final angle = atan2(
              circleCenter - (y + height / 2), circleCenter - (x + width / 2));
          // Calculate the new distance by decreasing it by a factor of 0.1 per update cycle.
          final newDistance = distance * styleOneSpeed;
          // Update the x and y coordinates of the small ball based on the new distance and angle.

          if (!showWinnerBlink) {
            if (!styleOneHitStoppper) {
              x += newDistance * cos(angle);
              y += newDistance * sin(angle);
            } else {
              x -= newDistance * cos(angle);
              y -= newDistance * sin(angle);
            }
          }
        }
      }
    } else {
      circleRadius = circleCenter + 25;
      middleBorder = circleCenter - (circleCenter * 0.3);
      speed = 10;
      ballToMiddle = 25;
      firstSlowDown = false;
      toSecondSlow = false;
      lap = 0;
      stopBallToDestionation = false;
      showWinnerBlink = false;
      styleOneHitStoppper = false;
      styleLapped = false;
      styleOneSpeed = 0.05;
      styleTwoHitStopper = false;
    }
  }

  @override
  void onCollisionStart(
      Set<Vector2> intersectionPoints, PositionComponent other) {
    if (other is StopperBallComponent) {
      lap++;
      speed -= 2.5;
    }
    if (other is BallDestionation) {
      if (lap == 1 && LiveGameManager.style == 1 && !styleLapped) {
        x = pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].x;
        y = pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].y;
        styleLapped = true;
        stopBallToDestionation = true;
      } else if (lap == 2 && LiveGameManager.style == 2) {
        if (styleTwoHitStopper) {
          add(TimerComponent(
              period: 0.1,
              onTick: () {
                x = pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)]
                    .x;
                y = pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)]
                    .y;
                stopBallToDestionation = true;
              }));
        }
      }

      if (styleOneHitStoppper) {
        add(TimerComponent(
            period: 0.2,
            onTick: () {
              x = pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].x;
              y = pointer[rouletteNumbers.indexOf(LiveGameManager.resultNum)].y;
              showWinnerBlink = true;
            }));
      }
    }

    if (other is StopperSOne) {
      styleOneHitStoppper = true;
      styleOneSpeed = 0.01;
    }

    if (other is StopperSTwo) {
      if (lap == 2) {
        styleTwoHitStopper = true;
        speed -= 1.5;
      }
    }

    super.onCollisionStart(intersectionPoints, other);
  }
}
