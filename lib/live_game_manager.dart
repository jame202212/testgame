import 'dart:math';

import 'package:flutter/cupertino.dart';

class LiveGameManager {
  /// 生成随机数
  static int randomInt({@required int min, @required int max}) {
    final random = Random();
    int next(int min, int max) => min + random.nextInt(max - min);
    var c = next(min, max);
    return c;
  }

  /// 固定中奖风格
  static void rouletteStart({@required int winCount, Function onFail}) {
    if (winCount > 36 || winCount < 0) {
      onFail("传入数字不符合规格，只接收0-36的整数，请检查逻辑");
      return;
    }
    LiveGameManager.resultNum = winCount;
    LiveGameManager.playAgain = true;
    LiveGameManager.style = randomInt(min: 1, max: 3);
  }

  static int resultNum = 0;
  static int style = 1;

  ///1,上下回弹，2，左右回弹 ，目前就两种

  static double circleOuterRadius = 390;
  static double circleWheelRadius = 300;

  static bool playAgain = true;
}
